package com.modelwindow;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Callback;

public class AddGroup_Controller implements Initializable {

	@FXML
	private Pane title;

	@FXML
	private Button close;

	@FXML
	private ImageView groupImage;

	@FXML
	private Button next;

	private Stage stage;

	@FXML
	void handleButtonAction(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
				stage = (Stage) close.getScene().getWindow();
				stage.close();
			} else if (button.equals(next)) {

			}
		}

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Image image = new Image(getClass().getClassLoader().getResource("images/group-default.png").toString(),200,200,true,true);
		setGroupImage(image);
	}
	
	private void setGroupImage(Image image){
		groupImage.setClip(new Circle(80,80,80));
		groupImage.setEffect(new DropShadow(20, Color.BLACK));
		groupImage.setImage(image);
	}

}
