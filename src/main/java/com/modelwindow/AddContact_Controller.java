package com.modelwindow;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Callback;

public class AddContact_Controller implements Initializable {

    @FXML
    private Button close;

    @FXML
    private Button add;
    
    @FXML
    private ListView memberList;
    
    @FXML
	private Pane title;
    private Stage stage;
    
    ObservableList<String> data = FXCollections.observableArrayList("Member 01", "Member 02", "Member 03", "Member 04", "Member 05", "Member 06" ,"Member 07");

    @FXML
    void handleButtonAction(ActionEvent event) {

    	if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
				stage = (Stage) close.getScene().getWindow();
				stage.close();
			}else if(button.equals(add)){
				
			}
		}
    	
    }
    
    private static class AttachmentListCell extends ListCell<String> {

		@Override
		public void updateItem(String item, boolean empty) {

			super.updateItem(item, empty);
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {

				ImageView pictureImageView = new ImageView();
				Image image = new Image(getClass().getClassLoader().getResource("images/user-default.png").toString(),50, 50, true, true);
				pictureImageView.setClip(new Circle(25, 25, 25));
				pictureImageView.setImage(image);

				setGraphic(pictureImageView);
				setText(item);
			}
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		memberList.setItems(data);
		memberList.getStyleClass().add("mylist");
		memberList.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> list) {
				return new AttachmentListCell();
			}
		});

		memberList.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

			}
		});
		
	}

}
