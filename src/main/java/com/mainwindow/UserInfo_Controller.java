package com.mainwindow;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.SnapshotParameters;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class UserInfo_Controller implements Initializable {
	@FXML 
	private ImageView userimage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		Image image = new Image(getClass().getClassLoader().getResource("images/h.jpg").toString(),200,200,true,true);
		
	    Circle c = new Circle();
	    c.setCenterX(80);
	    c.setCenterY(80);
	    c.setRadius(75);
		
	    DropShadow dropShadow = new DropShadow(); 
	    dropShadow.setBlurType(BlurType.GAUSSIAN);
	    dropShadow.setColor(Color.BLACK); 
	    //dropShadow.setWidth(20);
	    //dropShadow.setRadius(10); 
	    //dropShadow.setSpread(12);
	    
	    c.setEffect(dropShadow);
	    
		userimage.setClip(c);
		//userimage.setEffect(new DropShadow(20, Color.BLACK));
        userimage.setImage(image);
		
	}

}
