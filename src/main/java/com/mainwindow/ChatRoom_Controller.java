package com.mainwindow;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

public class ChatRoom_Controller implements Initializable {

	private static final long serialVersionUID = 1L;
	@FXML
	public Button send;
	@FXML
	public Button attachment;
	@FXML
	public ListView chatPane;
	@FXML
	public TextArea area;
	@FXML
	public Text chatMember;

	public HBox x;
	public VBox y;

	public ImageView pictureImageView;

	final FileChooser fileChooser = new FileChooser();

	String jid = null;
	boolean ismember;
	String time = "00:00 AM";

	@FXML
	protected void handleButtonAction(ActionEvent event) throws IOException {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(send)) {

				String message = area.getText();
				setMsg(message, jid, ismember);
				area.setText("");

			} else if (button.equals(attachment)) {
				System.out.println("Click on attachment button");
				configureFileChooser(fileChooser);
				File file = fileChooser.showOpenDialog(null);
				if (file != null) {
					System.out.println(file.getAbsolutePath());
					setImage(file.getAbsolutePath(), ismember);

				}
			}
		}
	}

	public void setjid(String jid) {
		this.jid = jid;
	}

	public void setRoomType(boolean value) {
		this.ismember = value;
	}

	public void setMsg(String msg, String jid, boolean isMember) {

		if (!msg.equals("")) {

			x = new HBox(10);
			x.setStyle("-fx-padding: 10 10 10 10;");

			y = new VBox(5);
			Label date = new Label();
			date.setText(time);
			date.setStyle("-fx-font-size: 8pt; -fx-text-fill: #454d58;");

			pictureImageView = new ImageView();
			pictureImageView.getStyleClass().add("bubble-img");

			Image image = null;
			// Image image = new Image(new ByteArrayInputStream(getAvatarImages(jid)), 50,
			// 50, true, true);
			if (isMember == true) {
				image = new Image(getClass().getClassLoader().getResource("images/user-default.png").toString(), 50, 50,
						true, true);
			} else {
				image = new Image(getClass().getClassLoader().getResource("images/group-default.png").toString(), 50,
						50, true, true);
			}
			pictureImageView.setClip(new Circle(25, 25, 25));
			pictureImageView.setImage(image);

			Label msgLabel = new Label();
			msgLabel.setStyle("-fx-text-fill: #FFFFFF;");
			msgLabel.setText(msg);
			msgLabel.getStyleClass().add("chat-bubble");
			x.setAlignment(Pos.TOP_RIGHT);

			y.setAlignment(Pos.TOP_RIGHT);
			y.getChildren().addAll(msgLabel, date);
			x.getChildren().addAll(y, pictureImageView);
			chatPane.getItems().add(x);
		}
	}

	public void setImage(String path, boolean isMember) {

		if (!path.equals("")) {

			x = new HBox(10);
			x.setStyle("-fx-padding: 10 10 10 10;");

			y = new VBox(5);
			Label date = new Label();
			date.setText(time);
			date.setStyle("-fx-font-size: 8pt; -fx-text-fill: #454d58;");

			pictureImageView = new ImageView();
			pictureImageView.getStyleClass().add("bubble-img");

			Image image = null;
			// Image image = new Image(new ByteArrayInputStream(getAvatarImages(jid)), 50,
			// 50, true, true);
			if (isMember == true) {
				image = new Image(getClass().getClassLoader().getResource("images/user-default.png").toString(), 50, 50,
						true, true);
			} else {
				image = new Image(getClass().getClassLoader().getResource("images/group-default.png").toString(), 50,
						50, true, true);
			}

			pictureImageView.setClip(new Circle(25, 25, 25));
			pictureImageView.setImage(image);

			Label imageLabel = new Label();
			imageLabel.setStyle("-fx-text-fill: #FFFFFF;");
			imageLabel.getStyleClass().add("chat-bubble");
			x.setAlignment(Pos.TOP_RIGHT);

			BufferedImage bufferedImage;
			try {
				bufferedImage = ImageIO.read(new File(path));
				Image _image = SwingFXUtils.toFXImage(bufferedImage, null);
				imageLabel.setGraphic(new ImageView(_image));
			} catch (IOException e) {
				e.printStackTrace();
			}

			y.setAlignment(Pos.TOP_RIGHT);
			y.getChildren().addAll(imageLabel, date);
			x.getChildren().addAll(y, pictureImageView);
			chatPane.getItems().add(x);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setChatMember(String text) {
		chatMember.setText(text);
	}

	private static void configureFileChooser(final FileChooser fileChooser) {
		fileChooser.setTitle("View Pictures");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Images", "*.*"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"), new FileChooser.ExtensionFilter("PNG", "*.png"));
	}

}
