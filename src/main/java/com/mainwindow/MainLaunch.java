package com.mainwindow;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainLaunch extends Application{
	
	private double xOffset = 0;
    private double yOffset = 0;

	public static void  main(String args[]) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		 try {
	            
	            Parent loader = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/MainWindow.fxml"));
				primaryStage.initStyle(StageStyle.TRANSPARENT);
				
				loader.setOnMousePressed(new EventHandler<MouseEvent>() {
					 
			            @Override
			            public void handle(MouseEvent event) {
			                xOffset = event.getSceneX();
			                yOffset = event.getSceneY();
			                
			            }
			        });
			        
				loader.setOnMouseDragged(new EventHandler<MouseEvent>() {
			            @Override
			            public void handle(MouseEvent event) {
			            	primaryStage.setX(event.getScreenX() - xOffset);
			            	primaryStage.setY(event.getScreenY() - yOffset);
			            }
			        });
				
				Scene scene = new Scene(loader,1000,600);
				primaryStage.setScene(scene);
				primaryStage.show();
          
	        } catch(Exception e) {
	            e.printStackTrace();
	        }
	}

}
