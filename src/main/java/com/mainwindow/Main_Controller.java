package com.mainwindow;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main_Controller implements Initializable {

	boolean isMaximize = false;
	double x, y;

	BufferedImage img = null;

	@FXML
	private Button close, maximize, minimize, chat, member, group, setting, send;
	@FXML
	private BorderPane bdr;
	@FXML
	private AnchorPane card;
	@FXML
	private Pane card2;
	@FXML
	private FlowPane p1;
	@FXML
	private Pane mainPane;
	@FXML
	public Pane commonPane;
	@FXML
	private TextField input;
	@FXML
	private ImageView userAtr;
	@FXML
	public HBox MainHbox;


	String currentUI = null;

	@FXML
	protected void handleButtonAction(ActionEvent event) {
		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
				Stage stage = (Stage) close.getScene().getWindow();
				stage.close();
			} else if (button.equals(maximize)) {
				Stage stage = (Stage) maximize.getScene().getWindow();
				if (isMaximize == false) {
					/*
					 * isMaximize = true; stage.setFullScreenExitHint("");
					 * stage.setFullScreen(true);
					 */
				} else {
					isMaximize = false;
					stage.setFullScreen(false);
				}
			} else if (button.equals(minimize)) {
				Stage stage = (Stage) minimize.getScene().getWindow();
				stage.setIconified(true);
			} else if (button.equals(chat)) {
				setCenter("chat");
			} else if (button.equals(member)) {
				setCenter("Member");
			} else if (button.equals(group)) {
				setCenter("Group");
			} else if (button.equals(setting)) {
				setCenter("setting");
			}
		}
	}

	public void SetUserAvatar() {
		Image image = new Image(getClass().getClassLoader().getResource("images/h.jpg").toString(), 100, 100,
				true, true);
		userAtr.setClip(new Circle(30, 30, 30));
		userAtr.setImage(image);
	}
	
	public void setCurrentUI(String ui) {
		this.currentUI = ui;
	}
	

	public void setCenter(String ui) {

		try {
			Parent root1 = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/" + ui + ".fxml"));
			MainHbox.getChildren().setAll(root1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void dragged(MouseEvent event) {
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setX(event.getSceneX() - x);
		stage.setY(event.getSceneY() - y);

	}

	@FXML
	void pressed(MouseEvent event) {

		x = event.getSceneX();
		y = event.getSceneY();

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		SetUserAvatar();
		setCenter("chat");

		userAtr.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				System.out.println("clicked on avatar ");
				
				
				
			}
		});

	}

}