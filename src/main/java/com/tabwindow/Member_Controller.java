package com.tabwindow;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.mainwindow.ChatRoom_Controller;
import com.sun.javafx.geom.Rectangle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.util.Callback;

public class Member_Controller implements Initializable {

	@FXML
	private ListView<String> lst;
	
	@FXML
	private Pane chatRoomPane;

	private Rectangle clip;

	static Map<String, String> mapMamber = new HashMap<String, String>();

	ObservableList<String> data = FXCollections.observableArrayList(
            "User 01", "User 02", "User 03", "User 04", "User 05");
	protected static List<byte[]> avatars = new ArrayList<byte[]>();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		lst.setItems(data);
		lst.getStyleClass().add("mylist");
		lst.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> list) {
				return new AttachmentListCell();
			}
		});

		lst.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				
				UpdateRoom(lst.getSelectionModel().getSelectedItem());
				
			}
		});

	}

	public void UpdateRoom(String name) {
		Parent root1 = null;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/ChatRoom.fxml"));
			root1 = (Parent) loader.load();
			ChatRoom_Controller room = loader.getController();
			room.setRoomType(true);
			room.setChatMember(name);

		} catch (IOException e) {
			e.printStackTrace();
		}
		chatRoomPane.getChildren().setAll(root1);
	}

	private static class AttachmentListCell extends ListCell<String> {

		@Override
		public void updateItem(String item, boolean empty) {

			super.updateItem(item, empty);
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {

				ImageView pictureImageView = new ImageView();
				Image image = new Image(getClass().getClassLoader().getResource("images/user-default.png").toString(),50,50,true,true);
				pictureImageView.setClip(new Circle(25, 25, 25));
				pictureImageView.setImage(image);

				setGraphic(pictureImageView);
				setText(item);
			}
		}
	}

	



}
