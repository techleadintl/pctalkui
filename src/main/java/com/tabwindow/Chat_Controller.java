package com.tabwindow;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.controlsfx.control.PopOver;

import com.mainwindow.ChatRoom_Controller;
import com.sun.javafx.geom.Rectangle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

public class Chat_Controller implements Initializable {

	@FXML
	private ListView<String> lst;

	@FXML
	public Pane chatRoomPane;

	@FXML
	private Button add;


	static Map<String, String> mapMamber = new HashMap<String, String>();

	ObservableList<String> data = FXCollections.observableArrayList("User 01", "User 02", "User 03", "User 04", "User 05");


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		setUserInfoPane();
		lst.setItems(data);
		lst.getStyleClass().add("mylist");
		lst.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> list) {
				return new AttachmentListCell();
			}
		});
		
		
		

		lst.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

				UpdateRoom(lst.getSelectionModel().getSelectedItem());

			}
		});

		add.setOnMousePressed(new EventHandler<MouseEvent>() {
			
			ContextMenu contextMenu = createPopup();
			@Override
			public void handle(MouseEvent event) {
				contextMenu.show(add,event.getScreenX(), event.getScreenY());
			}
		});

	}
	
	private ContextMenu createPopup() {
		final ContextMenu contextMenu = new ContextMenu();
		contextMenu.setStyle("-fx-background-radius: 3;-fx-effect: dropshadow(gaussian,rgba(0,0,0,0.5), 15, 0, 0, 15);");
		MenuItem member = new MenuItem("New User");
		MenuItem group = new MenuItem("New Group");
		
		member.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	popUp("NewUser");
            }
        });
		
		group.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	popUp("NewGroup");
            }
        });
		
		contextMenu.getItems().addAll(member, group);
		return contextMenu;
		
	}
	
	private void popUp(String model) {
		try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/" + model + ".fxml"));

            Stage stage = new Stage();
            stage.initStyle(StageStyle.TRANSPARENT);
            //stage.initOwner((Node)event.getSource()).getScene().getWindow();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setAlwaysOnTop(true);
            stage.setScene(new Scene(root, 320, 400));
            stage.showAndWait();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void UpdateRoom(String name) {
		Parent root1 = null;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/ChatRoom.fxml"));
			root1 = (Parent) loader.load();
			ChatRoom_Controller room = loader.getController();
			room.setRoomType(true);
			room.setChatMember(name);

		} catch (IOException e) {
			e.printStackTrace();
		}
		chatRoomPane.getChildren().setAll(root1);
	}

	public void setUserInfoPane() {
		Parent root1 = null;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/UserInfo.fxml"));
			root1 = (Parent) loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		chatRoomPane.getChildren().setAll(root1);
	}

	private static class AttachmentListCell extends ListCell<String> {

		@Override
		public void updateItem(String item, boolean empty) {

			super.updateItem(item, empty);
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {

				ImageView pictureImageView = new ImageView();
				Image image = new Image(getClass().getClassLoader().getResource("images/user-default.png").toString(),
						50, 50, true, true);
				pictureImageView.setClip(new Circle(25, 25, 25));
				pictureImageView.setImage(image);

				setGraphic(pictureImageView);
				setText(item);
			}
		}
	}

	@FXML
	void ActionListener(ActionEvent event) {
		
	}

	private VBox createPopupContent(final ImageView wiz) {
		final Label unfortunateEvent = new Label();
		unfortunateEvent.setWrapText(true);
		unfortunateEvent.setTextAlignment(TextAlignment.CENTER);
		final Button wand = new Button("Hello");
		final VBox wizBox = new VBox(5);
		wizBox.setAlignment(Pos.CENTER);
		wizBox.getChildren().setAll(wand, unfortunateEvent);
		wand.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {

			}
		});

		return wizBox;
	}
	
	/*Label lblName = new Label("John Doe");
    Label lblStreet = new Label("123 Hello Street");
    Label lblCityStateZip = new Label("MadeUpCity, XX 55555");   
    VBox vBox = new VBox(lblName, lblStreet, lblCityStateZip);
	PopOver popOver = new PopOver(vBox);
	Label label = new Label("Mouse mouse over me");
	popOver.show();*/

}
