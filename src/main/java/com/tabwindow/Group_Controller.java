package com.tabwindow;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.mainwindow.ChatRoom_Controller;
import com.sun.javafx.geom.Rectangle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import javafx.scene.shape.*;

public class Group_Controller implements Initializable {

	@FXML 
	private ListView<String> group_lst;
	
	@FXML
	private Pane chatRoomPane;
	
	private Rectangle clip;
	
	ObservableList<String> data = FXCollections.observableArrayList(
            "Group 01", "Group 02", "Group 03", "Group 04", "Group 05");
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		group_lst.setItems(data);
		group_lst.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
        @Override
        public ListCell<String> call(ListView<String> list) {
            return new AttachmentListCell();
        }
    });
		
		group_lst.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				
				UpdateRoom(group_lst.getSelectionModel().getSelectedItem());
				
			}
		});
		
	}
	
	private static class AttachmentListCell extends ListCell<String> {
		
        @Override
        public void updateItem(String item, boolean empty) {
        	
            super.updateItem(item, empty);
            if (empty) {
                setGraphic(null);
                setText(null);
            } else {
                ImageView pictureImageView = new ImageView();
				Image image = new Image(getClass().getClassLoader().getResource("images/group-default.png").toString(),50,50,true,true);
                pictureImageView.setClip(new Circle(25,25,25));
                pictureImageView.setImage(image);
                
                setGraphic(pictureImageView);
                setText(item);
            }
        }
    }
	
	public void UpdateRoom(String name) {
		Parent root1 = null;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/ChatRoom.fxml"));
			root1 = (Parent) loader.load();
			ChatRoom_Controller room = loader.getController();
			room.setRoomType(false);
			room.setChatMember(name);

		} catch (IOException e) {
			e.printStackTrace();
		}
		chatRoomPane.getChildren().setAll(root1);
	}
	
	

}

