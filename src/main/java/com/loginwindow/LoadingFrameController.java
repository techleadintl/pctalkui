package com.loginwindow;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LoadingFrameController {
	
	@FXML private AnchorPane login_frame;
	
	@FXML private Button close, minimize;

	@FXML private ProgressBar prograssbar_loading;

	@FXML
	protected void handleButtonAction(ActionEvent event) {
		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
				
				Platform.exit();
			
			} else if (button.equals(minimize)) {
				
				Stage stage = (Stage) minimize.getScene().getWindow();
				stage.setIconified(true);
		
			}
		}
	}

}
