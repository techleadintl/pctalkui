package com.loginwindow;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FxRunner extends Application{

	 private double xOffset = 0;
	 private double yOffset = 0;
	
	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/LoginFrame.fxml"));

		
		primaryStage.initStyle(StageStyle.TRANSPARENT);
				 
		root.setOnMousePressed(new EventHandler<MouseEvent>() {
	           

				@Override
	            public void handle(MouseEvent event) {
	                xOffset = event.getSceneX();
	                yOffset = event.getSceneY();
	            }
	        });
	        
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
            	primaryStage.setX(event.getScreenX() - xOffset);
            	primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        
		Scene scene = new Scene(root,538,475);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}

}
