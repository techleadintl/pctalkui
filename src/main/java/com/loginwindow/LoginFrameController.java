package com.loginwindow;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LoginFrameController implements Initializable{
	
	@FXML private Button close, minimize, signIn_loging;
	
	@FXML private AnchorPane login_frame, anchorpane_loading;
	
	@FXML private TextField mobile_number_loging;
	
	@FXML public PasswordField password_loging;

	@FXML private Hyperlink create_one;
	
	@FXML private CheckBox save_password;

	@FXML CheckBox auto_login;
	
	@FXML private ComboBox<String> cmb;

	
	@FXML
	public void actionButton(ActionEvent e) throws Exception{

		if (cmb.getSelectionModel().isEmpty()|| mobile_number_loging.getText().isEmpty() || password_loging.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Login");
			alert.setHeaderText(null);
			alert.setContentText("Please Fill All Fields !");
			ButtonType buttonTypeOne = new ButtonType("Ok");
		
			alert.getButtonTypes().setAll(buttonTypeOne);
			Optional<ButtonType> result = alert.showAndWait();
			
		}else {
			
			AnchorPane loading_root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/LoadingFrame.fxml"));  
			login_frame.getChildren().setAll(loading_root);
			
			if(!save_password.isSelected()) {
				auto_login.setSelected(false);
			}else if (auto_login.isSelected()) {
				save_password.setSelected(true);
			}
			
			/*LoginDialog log = new LoginDialog();
	  		log.login(cmb.getValue(),mobile_number_loging.getText(), password_loging.getText(), save_password.isSelected(), auto_login.isSelected(),loading_root);
*/
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		System.out.println("bbbbbbbbbbbbb");
		/*Map<String, VCard> vCardMap = TalkManager.getVCardMap();

		String lastUsername = TalkManager.getAppConfig().getLastUsername();

		VCard personalVCard = vCardMap.get(lastUsername);

		if (personalVCard == null && !vCardMap.isEmpty()) {
			personalVCard = new ArrayList<>(vCardMap.values()).get(0);
		}

		setVCard(personalVCard);
		
		if (auto_login.isSelected() && save_password.isSelected()) {
			AnchorPane loading_root;
			try {
				loading_root = FXMLLoader.load(getClass().getResource("LoadingFrame.fxml"));
				login_frame.getChildren().setAll(loading_root);
			} catch (IOException e) {
				e.printStackTrace();
			}  
			
			
			if(!save_password.isSelected()) {
				auto_login.setSelected(false);
			}else if (auto_login.isSelected()) {
				save_password.setSelected(true);
			}
			LoginDialog log = new LoginDialog();
	  		log.login(cmb.getValue(),mobile_number_loging.getText(), password_loging.getText(), save_password.isSelected(), auto_login.isSelected(),login_frame);
		}
		
		if (lastUsername.trim().length() > 0) {
			Platform.runLater(new Runnable() {
                @Override
                public void run() {
                	password_loging.requestFocus();
                }
            });
		}*/
	}
	
/*	public void setVCard(VCard vcard) {
		if (vcard == null) {
			return;
		}
	
		int countryCode = vcard.getCountryCode();
		String code = String.valueOf(countryCode);
		String tel = vcard.getTel().substring(("00" + code).length());
		cmb.setValue(code);
		mobile_number_loging.setText(tel);

		if (vcard.isSavePassword()) {
			String password = vcard.getPassword();
			if (password != null) {
				password_loging.setText(password);
			}
			save_password.setSelected(true);
		}
	
		if (vcard.isAutoLogin()) {
			auto_login.setSelected(vcard.isAutoLogin());
		}
	}
	
	@FXML
	public void actionPerformed(ActionEvent e) {
		
		SwingUtil.openBrowseURL(Res.getProperty("talk.register"));	
		
	}*/
	
	@FXML
	protected void handleButtonAction(ActionEvent event) {
		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
				
				Platform.exit();
			
			} else if (button.equals(minimize)) {
				
				Stage stage = (Stage) minimize.getScene().getWindow();
				stage.setIconified(true);
		
			}
		}
	}
	@FXML
	public boolean isSavePassword() {
		return false;
		
	}
	@FXML
	public void isAutoLogin() {
		
	}
	@FXML
	public void getPassword() {
		new String(password_loging.getText());
	}

	public void call(KeyEvent e) {
		ComboBoxAutoComplete cb = new ComboBoxAutoComplete(cmb);
		cb.handleOnKeyPressed(e);
	}
	
	
}
