package com.settingwindow;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.sun.javafx.scene.control.skin.LabeledText;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

public class Setting_Controller implements Initializable {

	@FXML
	private ListView<String> Setting_lst;
	@FXML
	private Pane uppur;
	@FXML
	Pane SettingPane;

	ObservableList<String> data = FXCollections.observableArrayList("General Setting", "Shortcut Keys","Language Setting", "About");

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setRoom("GenaralSettings");
		Setting_lst.setItems(data);
		Setting_lst.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> list) {
				return new AttachmentListCell();
			}
		});

		Setting_lst.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (Setting_lst.getSelectionModel().getSelectedItem().equals("General Setting")) {
					setRoom("GenaralSettings");
				} else if (Setting_lst.getSelectionModel().getSelectedItem().equals("Shortcut Keys")) {
					setRoom("ShortcutKeys");
				} else if (Setting_lst.getSelectionModel().getSelectedItem().equals("Language Setting")) {
					setRoom("LanguageSettings");
				} else if(Setting_lst.getSelectionModel().getSelectedItem().equals("About")){
					setRoom("About");
				}
			}
		});

	}

	private static class AttachmentListCell extends ListCell<String> {
		@Override
		public void updateItem(String item, boolean empty) {
			super.updateItem(item, empty);
			this.setStyle("-fx-cell-size: 60;");
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {
				ImageView pictureImageView = new ImageView();
				Image image = new Image(getClass().getClassLoader().getResource("images/" + item + ".png").toString());
				pictureImageView.setImage(image);
				setGraphic(pictureImageView);
				setText(item);
			}
		}
	}

	public void setRoom(String ui) {
		Parent root1 = null;
		try {
			root1 = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/" + ui + ".fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		SettingPane.getChildren().setAll(root1);
	}

}
